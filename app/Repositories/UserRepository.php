<?php


namespace App\Repositories;


use App\User;
use Illuminate\Support\Facades\DB;

class UserRepository
{


    public function getSummary( $type = '' )
    {

        return  $summary = [
            'user_counts' => User::all()->count(),
            'new_users'   => User::newUsers()->count()
        ];
    }

}
