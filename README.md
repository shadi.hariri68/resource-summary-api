## Resource summary API
This is a sample repository for [article](https://medium.com/@shadi.hariri68/resource-summary-rest-api-with-laravel-using-repository-b7ffcbc8ef1?sk=3cea850ef7da9147d4c08e91839398dd
).


## Tech/framework used
* Laravel

## Installation
* composer install
* setup .env for database connection
* php artisan migrate


